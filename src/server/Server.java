package server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
	private static int PORT = 1337;

	// Server stuff
	private int serverPort = 0;
	private ServerSocket serverSock = null;

	// Client stuff
	private ArrayList<ClientListener> clientList;
	private int noClients = 0;

	/**
	 * Constructor
	 * @param serverPort Port
	 */
	public Server(int port) {
		this.serverPort = port;
		clientList = new ArrayList<ClientListener>();
		try {
			this.serverSock = new ServerSocket(serverPort);
			System.out.println("Socket initierad.");
		}
		catch (IOException e){
			e.printStackTrace(System.err);
		}
	}

	/**
	 * Handles the connections with the clients.
	 * TODO: tr�da 
	 */
	public void waitForConnections() {
		(new Thread() {
			public void run() {

				int clientID = 0;
				while(true) {
					Socket sock = null;
					try {
						sock = serverSock.accept();
						ClientListener temp = new ClientListener(sock, clientID++);
						clientList.add(temp);
						(new Thread(temp)).start();

					} catch (IOException e) {
						e.printStackTrace();
					}

				}
			}
		}).start();
	}

	private void broadcast(String str) {
		for (ClientListener cli : clientList) {
			byte[] data = str.getBytes();
			byte[] buf = new byte[data.length];
			try {
				cli.sockOutput.write(data, 0, data.length);
			} catch (IOException e) {
				System.err.println("N�got fel.");
//				e.printStackTrace();
			}
		}
	}

	/**
	 * Main function.
	 */
	public static void main(String[] args) {
		System.out.println("Server uppe p� port " + PORT + ".");
		Server server = new Server(PORT);
		server.waitForConnections();
	}


	private class ClientListener implements Runnable {
		private Socket clientSocket;
		private int ID;
		InputStream sockInput = null;
		OutputStream sockOutput = null;
		private boolean loggedOn = true;
		private int bytes_read;

		private ClientListener(Socket sock, int ID) {
			this.clientSocket = sock;
			this.ID = ID;
			try {
				sockInput = clientSocket.getInputStream();
				sockOutput = clientSocket.getOutputStream();
				System.out.println("Klient skapad. ID = " + ID);
			} catch (IOException e) {
				System.err.println("Problem med att �ppna inputstreamsen.");
			}
		}

		@Override
		public void run() {
			while(true) {
				byte[] buf=new byte[1024];
				int bytes_read = 0;
				try {
					Thread.sleep(100);
					bytes_read = sockInput.read(buf, 0, buf.length);
					String resp = new String(buf, 0, bytes_read);
					System.out.println(resp);
					broadcast("Klient " + ID + ": " + resp);
				} catch (StringIndexOutOfBoundsException e) {
					clientList.remove(this);
					System.out.println("Klient " + ID + "l�mnade chatten.");
					break;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}