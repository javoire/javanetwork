package client.gui;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import client.Client;

public class ClientView implements Runnable {
	
    public JTextArea 	chatMessages;
    public JTextField 	inputField;
    public JButton 		sendButton;
    public JScrollPane 	chatScrollWrapper;
    public String 		messageBuffer; 

	public void run() {
        JFrame mainFrame 	= new JFrame ("Hello, World!");
        chatMessages 		= new JTextArea();
        chatScrollWrapper 	= new JScrollPane(chatMessages);
        inputField 			= new JTextField("Skriv n�got!");
        sendButton 			= new JButton("Skicka");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container mainPane = mainFrame.getContentPane();
        mainPane.setLayout(new BorderLayout());
     
        sendButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent arg0) {
        		sendMessage(messageBuffer);
        	}
        });
        inputField.addKeyListener(new KeyListener() {
        	@Override
        	public void keyReleased(KeyEvent e) {
        		if(e.getKeyCode() == KeyEvent.VK_ENTER) {
        			messageBuffer = inputField.getText();
        			sendMessage(messageBuffer);
        		}
        	}
        	
        	@Override
        	public void keyPressed(KeyEvent e) {
        	}
        	
        	@Override
        	public void keyTyped(KeyEvent e) {
        	}
        });
        inputField.addFocusListener(new FocusListener() {
        	@Override
        	public void focusGained(FocusEvent e) {
        		if(inputField.getText().equals("Skriv n�got!")) {
        			inputField.setText("");        			
        		}
        	}
        	
        	@Override
        	public void focusLost(FocusEvent e) {
        		messageBuffer = inputField.getText();
        		if(inputField.getText().equals("")) {
        			inputField.setText("Skriv n�got!");
        		}
        	}
        });
        
        chatMessages.setEditable(false);
        chatScrollWrapper.setPreferredSize(new Dimension(600, 600));
        inputField.setPreferredSize(new Dimension(550, 40));
        mainPane.add(chatScrollWrapper, BorderLayout.PAGE_START);
        mainPane.add(inputField, BorderLayout.LINE_START);
        mainPane.add(sendButton, BorderLayout.CENTER);
        mainFrame.pack();
        mainFrame.setVisible(true);
	}
	
	/**
	 * Gets the current chatmessage from the chat input field
	 * @return String chatmessage
	 */
	public String getMessageBuffer() {
		return messageBuffer;
	}
	
	/**
	 * Skicka till server
	 */
	public void sendMessage(String str) {
		if (str.equals("")) {
			return;
		}
		Client.CONNECTION.send(str); // G�r inget med argumentet f�r tillf�lle
		inputField.setText("");
	}
	
	/**
	 * Skriva i meddelandet.
	 */
	public void appendNewChatMessage(String str) {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		str = "[" + dateFormat.format(date) + "] " + str + "\n";
		chatMessages.append(str);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ClientView se = new ClientView();
		SwingUtilities.invokeLater(se);
	}
}
