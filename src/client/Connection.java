package client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;

public class Connection {

	private String hostname;
	private int port;
	private boolean isConnected = true;

	private Socket sock = null;
	private InputStream sockInput = null;
	private OutputStream sockOutput = null;
	private String broadcast = "";


	public Connection(String host, int port) {
		this.hostname = host;
		this.port = port;

		openConnection();
		messageListener();
	}

	/** 
	 * Ansluter till servern.
	 */
	public void openConnection() {
		try {
			sock = new Socket(hostname, port);
			sockInput = sock.getInputStream();
			sockOutput = sock.getOutputStream();
//			sock.setSoTimeout(50);
			System.out.println("Ansluten till "+hostname+":"+port);
		}
		catch (IOException e){
			System.err.println("Gick inte att ansluta till servern.");
			System.exit(1);
		}
	}

	/**
	 * Skickar meddelanden till servern.
	 */
	public void send(String str) {
//		(new Thread() {
//			public void run() {
//				String get = Client.GUI.getMessage();
				byte[] data = str.getBytes();
				try {
					sockOutput.write(data, 0, data.length);
				}
				catch (IOException e){
					System.err.println("Problem med att skicka data.");
				}
//			}
//		}).start();
	}

	/**
	 * Lyssnar p� uppdateringar.
	 */
	public void messageListener() {
		(new Thread() {
			public void run() {
				while(true) {
					byte[] buf=new byte[1024];
					int bytes_read = 0;
					try {
						Thread.sleep(100);
						bytes_read = sockInput.read(buf, 0, buf.length);
						broadcast = new String(buf, 0, bytes_read);
						System.out.println(broadcast);
						Client.GUI.appendNewChatMessage(broadcast);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
	
	public void scanNetwork() {
		int threads = 32;
		try {
			sock.setSoTimeout(30);
		} catch (SocketException e) {
			e.printStackTrace();
		}
		
		for (int i = 0; i < threads; i++) {
		}
	}
}
