package client;

import javax.swing.SwingUtilities;

import client.gui.ClientView;

public class Client {
	
	private static String HOST = "localhost";
	private static int PORT = 1337;
	public static ClientView GUI;
	public static Connection CONNECTION;

	public static void main(String[] args) {
		CONNECTION = new Connection(HOST, PORT);
		GUI = new ClientView();
		SwingUtilities.invokeLater(GUI);
	}
}
